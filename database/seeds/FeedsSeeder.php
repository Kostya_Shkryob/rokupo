<?php

use Illuminate\Database\Seeder;

class FeedsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Feed::truncate();
        App\Post::truncate();
        factory(App\Feed::class, 50)->create()->each(function ($feed) {
            factory(App\Post::class, 150)->create(['feed_id' => $feed->_id]);
        });
    }
}
