<?php

use Faker\Generator as Generator;

$factory->define(App\Post::class, function (Generator $generator) {
    gc_collect_cycles();
    
    $faker = Faker\Factory::create('en_US');

    return [
//        'feed_id' => $feedID,
        'title' => $faker->realText(), // Item title
        'url' => $faker->url(), // Item url
        'published_date' => $faker->dateTime(), // Item published date (DateTime object)
        'updated_date' => $faker->dateTime(), // Item updated date (DateTime object)
        'language' => 'en_US', // Item language
        'author' => $faker->firstName . ' ' . $faker->lastName, // Item author
        'enclosure_url' => $faker->imageUrl(), // Enclosure url
        'enclosure_type' => 'image/png', // Enclosure mime-type (audio/mp3, image/png...)
        'content' => $faker->randomHtml(2,3), // Item content (filtered or raw)
        'is_rtl' => false, // Return true if the item language is Right-To-Left
        'parsed_date' => \Carbon\Carbon::now()
    ];
});
