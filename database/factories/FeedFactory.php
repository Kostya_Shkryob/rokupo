<?php

use Faker\Generator as Generator;

$factory->define(App\Feed::class, function (Generator $generator) {
    gc_collect_cycles();

    $faker = Faker\Factory::create('en_US');

    return [
        'title' => $faker->realText(75),
        'url' => $faker->url(),
        'website' => $faker->url(),
        'last_update' => $faker->dateTime(),
        'language' => 'en_US',
        'description' => $faker->realText(),
        'logo' => $faker->imageUrl(),
        'last_parsed' => \Carbon\Carbon::now()
    ];
});
