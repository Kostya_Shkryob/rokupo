<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedTextIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('feeds', function (Blueprint $collection) {
            $collection->text(['title', 'url', 'website', 'description']);

            $collection->index(
                [
                    'title' => 'text',
                    'url' => 'text',
                    'website' => 'text',
                    'description' => 'text'
                ],
                'feed_full_text',
                null,
                [
                    'weights' => [
                        'title' => 30,
                        'url' => 20,
                        'website' => 20,
                        'description' => 10
                    ],
                    'name' => 'feed_full_text',
                    'default_language' => 'en',
                    'language_override' => 'en' 
                ]
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('feeds', function (Blueprint $collection) {
            $collection->dropIndex(['feed_full_text']);
        });
    }
}
