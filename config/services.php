<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '474851166248827',
        'client_secret' => '9851e7d6c94d02641dc225d6c6e24273',
        'redirect' => '/login/facebook/callback'
    ],

    'twitter' => [
        'client_id' => 'zrSjUQdLRATRH8G7K5cqqKdzC',
        'client_secret' => 'gdwGTFAe9AfXQG80N7Al5nEwIAKNeRG2Yc1QLFHxlhKQ6jqRcI',
        'redirect' => '/login/twitter/callback'
    ],

    'linkedin' => [
        'client_id' => '779lpiis23opwv',
        'client_secret' => 'tnRTWE9uG6c9rgGn',
        'redirect' => '/login/linkedin/callback'
    ],

    'google' => [
        'client_id' => '39787180912-cu02t2oate0bbg61vg435cpfjh1b3035.apps.googleusercontent.com',
        'client_secret' => 'OJLDSMma5-b2XbVJR6qLqcj3',
        'redirect' => '/login/google/callback'
    ]
];
