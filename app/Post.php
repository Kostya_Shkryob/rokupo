<?php

namespace App;

use Carbon\Carbon;
use Jenssegers\Mongodb\Auth\User as MongoUser;

class Post extends MongoUser
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'feed_id', 'title', 'url', 'parsed_date', 'published_date', 'updated_date', 'language', 'author',
        'enclosure_url', 'enclosure_type', 'content', 'is_rtl', 'parsed_date'
    ];

    public static function parse($feedID, $item)
    {
        $post = Post::where([
            'feed_id' => $feedID,
            'auth_id' => $item->getUrl()
        ])->first();

        if (!$post) {
            $data = [
                'feed_id' => $feedID,
                'title' => $item->getTitle(), // Item title
                'url' => $item->getUrl(), // Item url
                'published_date' => $item->getPublishedDate(), // Item published date (DateTime object)
                'updated_date' => $item->getUpdatedDate(), // Item updated date (DateTime object)
                'language' => $item->getLanguage(), // Item language
                'author' => $item->getAuthor(), // Item author
                'enclosure_url' => $item->getEnclosureUrl(), // Enclosure url
                'enclosure_type' => $item->getEnclosureType(), // Enclosure mime-type (audio/mp3, image/png...)
                'content' => $item->getContent(), // Item content (filtered or raw)
                'is_rtl' => $item->isRTL(), // Return true if the item language is Right-To-Left
                'parsed_date' => Carbon::now()
            ];

            $post = Post::create($data);
            $post->new = true;
        } else {
            $post->new = false;
        }

        return $post;
    }

    public static function getLast($feedID, $count=15)
    {
        return Post::where('feed_id', $feedID)->limit($count);
    }

}
