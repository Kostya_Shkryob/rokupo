<?php

namespace App;

use Jenssegers\Mongodb\Auth\User as MongoUser;

class Subscription extends MongoUser
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'feed_id', 'unread_posts', 'title'
    ];

}
