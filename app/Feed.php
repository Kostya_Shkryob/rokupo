<?php

namespace App;

use Jenssegers\Mongodb\Auth\User as MongoUser;
use PicoFeed\Reader\Reader;
use Carbon\Carbon;

class Feed extends MongoUser
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'url', 'website', 'last_update', 'language', 'description', 'logo', 'last_parsed'
    ];

    public static function parse($url)
    {
        $feed = false;
        try {

            $reader = new Reader;

            // Return a resource
            $resource = $reader->download($url);

            // Return the right parser instance according to the feed format
            $parser = $reader->getParser(
                $resource->getUrl(),
                $resource->getContent(),
                $resource->getEncoding()
            );

            // Return a Feed object
            $rssFeed = $parser->execute();

            $data = [
                'title' => $rssFeed->getTitle(), // Feed title
                'url' => $rssFeed->getFeedUrl(), // Feed url
                'website' => $rssFeed->getSiteUrl(), // Website url
                'last_update' => $rssFeed->getDate(), // Feed last updated date (DateTime object)
                'language' => $rssFeed->getLanguage(), // Feed language
                'description' => $rssFeed->getDescription(), // Feed description
                'logo' => $rssFeed->getLogo(), // Feed logo (can be a large image, different from icon)
                'last_parsed' => Carbon::now()
            ];

            $feed = Feed::create($data);

            $items = $rssFeed->getItems(); // List of item objects

            foreach ($items as $item) {
                Post::parse($feed->id, $item);
            }
            // Print the feed properties with the magic method __toString()
        } catch (PicoFeedException $e) {
            // Do Something...
        }
        return $feed;
    }

    public function posts() {
        return $this->hasMany('App\Post');
    }
}
