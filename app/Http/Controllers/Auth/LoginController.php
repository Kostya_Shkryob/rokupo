<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use Socialite;
use App\Http\Controllers\Controller;
use App\User;

class LoginController extends Controller
{
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $authInfo = Socialite::driver($provider)->user();

        $user = User::where([
            'auth_provider' => $provider,
            'auth_id' => $authInfo->id
        ])->first();

        if (!$user) {
            $user = User::create([
                'auth_provider' => $provider,
                'auth_id' => $authInfo->getId(),
                'nickname' => $authInfo->getNickname(),
                'name' => $authInfo->getName(),
                'email' => $authInfo->getEmail(),
                'avatar' => $authInfo->getAvatar(),
            ]);
        }

        Auth::login($user);

        return redirect('/');
    }
}