<?php

namespace App\Http\Controllers;

use App\Events\PostWasRead;
use App\Events\SubscriptionCreated;
use App\Events\SubscriptionRemoved;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Socialite;
use App\Feed;
use App\Subscription;

class SubscriptionController extends Controller
{
    public function add(Request $request)
    {
        $user = Auth::guard('web')->user();

        if (!$user) {
            return response()->json(['error' => 'Please login first'], 401);
        }

        $url = $request->input('url');

        $feed = Feed::where([
            'url' => $url
        ])->first();

        if (!$feed) {
            $feed = Feed::parse($url);
        }

        if (!$feed) {
            return response()->json(['error' => 'Can not parse URL'], 400);
        }

        $lastIDs = Post::getLast($feed->id)->pluck('_id');

        $subscription = Subscription::create([
            'user_id' => $user->id,
            'feed_id' => $feed->id,
            'title' => $feed->title,
            'unread_posts' => $lastIDs->toArray()
        ]);

        event(new SubscriptionCreated($user->id, $subscription));

        return response()->json($subscription, 200);
    }

    public function index(Request $request)
    {
        $user = Auth::guard('web')->user();

        if (!$user) {
            return response()->json(['error' => 'Please login first'], 401);
        }

        $subscriptions = Subscription::where('user_id', $user->id)->get();

        return response()->json($subscriptions, 200);
    }

    public function delete($id)
    {
        $user = Auth::guard('web')->user();

        if (!$user) {
            return response()->json(['error' => 'Please login first'], 401);
        }

        $subscription = Subscription::where('_id', $id)->first();

        if (!$subscription) {
            return response()->json(['error' => 'Subscription was not found'], 404);
        }

        if ($subscription['user_id'] !== $user->id) {
            return response()->json(['error' => 'You are not allowed to delete this subscription'], 401);
        }

        $result = $subscription->delete();

        event(new SubscriptionRemoved($user->id, $id));

        return response()->json($result, 200);
    }

    public function markRead(Request $request)
    {
        $user = Auth::guard('web')->user();

        if (!$user) {
            return response()->json(['error' => 'Please login first'], 401);
        }

        $feedID = $request->input('feed_id');

        if (!$feedID) {
            return response()->json(['error' => 'Please provide feed_id'], 400);
        }

        $subscription = Subscription::where('feed_id', $feedID)
            ->where('user_id', $user->id)
            ->first();

        if (!$subscription) {
            return response()->json(['error' => 'Subscription was not found'], 404);
        }

        $postID = $request->input('post_id');

        if (!$postID) {
            return response()->json(['error' => 'Please provide post_id'], 400);
        }

        $result = \DB::collection('subscriptions')->where('feed_id', $feedID)
            ->where('user_id', $user->id)
            ->pull('unread_posts', $postID);

        event(new PostWasRead($user->id, $postID, $feedID));

        return response()->json($result, 200);
    }
}