<?php

namespace App\Http\Controllers;

use App\Feed;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Subscription;

class PostController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::guard('web')->user();

        $query = Post::query();
        $feedID = null;

        if ($request->query('subscription')) {
            $subscription = Subscription::where('_id', $request->query('subscription'))->first();

            If (!$subscription) {
                return response()->json(['error' => 'Subscription was not found'], 404);
            }

            if (!$user || $subscription['user_id'] !== $user->id) {
                return response()->json(['error' => 'You are not allowed to view this subscription'], 401);
            }

            $query->where('feed_id', $subscription['feed_id'])
                ->whereIn('_id', $subscription['unread_posts']);
            $feedID = $subscription['feed_id'];
        }

        if ($request->query('feed')) {
            $query->where('feed_id', $request->query('feed'));
            $feedID = $request->query('feed');
        }

        $result = $query->paginate();

        if ($feedID) {
            $result = collect(['feed' => Feed::find($feedID)])->merge($result);
        }

        return response()->json($result, 200);
    }
}