<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class UserController extends Controller
{
    public function profile(Request $request)
    {
        $user = Auth::guard('web')->user();

        if (!$user) {
            return response()->json(null, 401);
        }

        if ($request->isMethod('post') || $request->isMethod('put')) {
            $result = User::where('auth_provider', $user->auth_provider)
                ->where('auth_id', $user->auth_id)
                ->update([
                    'nickname' => $request->input('nickname'),
                    'name' => $request->input('name'),
                    'email' => $request->input('email')
                ]);

            $user = User::where([
                'auth_provider' => $user->auth_provider,
                'auth_id' => $user->auth_id
            ])->first();

            Auth::login($user);
        }

        return response()->json($user, 200);
    }
}