<?php

namespace App\Http\Controllers;

use App\Feed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Socialite;

class FeedController extends Controller
{
    public function index(Request $request)
    {
        $query = Feed::query();

        if ($request->query('query')) {
            $query->orderBy('score', ['$meta' => 'textScore']);
            $query->whereRaw(['$text' => ['$search' => $request->query('query')]]);
        }

        $result = $query->paginate();

        return response()->json($result, 200);
    }

    public function suggestions()
    {
        $query = Feed::query();
        $query->with([
            'posts' => function ($query) {
                // Correctly limits the number of threads to 10.
                $query
                    ->take(10);
            }
        ]);
        $result = $query->paginate();
        return response()->json($result, 200);
    }
}