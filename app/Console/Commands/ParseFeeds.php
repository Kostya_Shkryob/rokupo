<?php

namespace App\Console\Commands;

use App\Feed;
use App\Post;
use Illuminate\Console\Command;
use PicoFeed\Reader\Reader;

class ParseFeeds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feeds:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parses RSS feeds';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Feed::chunk(20, function ($feeds) {
            foreach ($feeds as $feed) {
                $reader = new Reader();

                // Return a resource
                $resource = $reader->download($feed->url);

                // Return the right parser instance according to the feed format
                $parser = $reader->getParser(
                    $resource->getUrl(),
                    $resource->getContent(),
                    $resource->getEncoding()
                );

                // Return a Feed object
                $rssFeed = $parser->execute();

                $items = $rssFeed->getItems(); // List of item objects

                $newPosts = [];
                foreach ($items as $item) {
                    $post = Post::parse($feed->id, $item);
                    $newPosts[] = $post->id;
                }
            }
        });;
    }
}
