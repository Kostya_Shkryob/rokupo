<?php

namespace App;

use Jenssegers\Mongodb\Auth\User as MongoUser;

class User extends MongoUser
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'auth_provider', 'auth_id', 'nickname', 'name', 'email', 'avatar',
    ];

}
