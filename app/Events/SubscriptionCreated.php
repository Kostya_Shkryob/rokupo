<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SubscriptionCreated implements ShouldBroadcast
{
    use SerializesModels;

    public $userID;
    public $subscription;

    public function __construct($userID, $subscription)
    {
        $this->userID = $userID;
        $this->subscription = $subscription;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('user.' . $this->userID);
    }
}