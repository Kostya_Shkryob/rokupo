<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SubscriptionRemoved implements ShouldBroadcast
{
    use SerializesModels;

    public $userID;
    public $subscriptionID;

    public function __construct($userID, $subscriptionID)
    {
        $this->userID = $userID;
        $this->subscriptionID = $subscriptionID;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('user.' . $this->userID);
    }
}