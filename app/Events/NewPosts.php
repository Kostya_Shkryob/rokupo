<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewPosts implements ShouldBroadcast
{
    public $userID;
    public $feedID;
    public $posts;

    public function __construct($userID, $feedID, $posts)
    {
        $this->userID = $userID;
        $this->feedID = $feedID;
        $this->posts = $posts;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('user.' . $this->userID);
    }
}