<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PostWasRead implements ShouldBroadcast
{
    public $postID;
    public $userID;
    public $feedID;

    public function __construct($userID, $postID, $feedID)
    {
        $this->userID = $userID;
        $this->postID = $postID;
        $this->feedID = $feedID;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('user.' . $this->userID);
    }
}