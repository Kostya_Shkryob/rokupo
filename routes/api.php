<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::any('profile', 'UserController@profile');

Route::post('subscriptions', 'SubscriptionController@add');
Route::get('subscriptions', 'SubscriptionController@index');
Route::delete('subscriptions/{id}', 'SubscriptionController@delete');
Route::put('subscriptions/read', 'SubscriptionController@markRead');

Route::get('posts', 'PostController@index');

Route::get('feeds', 'FeedController@index');
Route::get('feeds/suggestions', 'FeedController@suggestions');