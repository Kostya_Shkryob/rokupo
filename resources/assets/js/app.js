
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('popper.js/dist/umd/popper');
require('./bootstrap');

window.Vue = require('vue');

require('./filters');
const VueResource = require('vue-resource').default;
const VueInView = require('vue-inview');
const moment = require('moment');
const Vuex = require('vuex');
const router = require('./routes');

Vue.use(VueResource);
Vue.use(VueInView);
Vue.use(Vuex);

Vue.component('my-header', require('./components/MyHeader.vue'));
Vue.component('post', require('./components/post/View.vue'));

const store = require('./store');

const app = new Vue({
    data: {
        user: store
    },
    created() {
        this.$store.dispatch('getProfile');
        this.$store.dispatch('getConfig');
    },
    store: store,
    el: '#app',
    router
});

Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('YYYY-MM-DD hh:mm')
    }
});
