const Vuex = require('vuex');
const Echo = require('laravel-echo');
const Pusher = require('pusher-js');

const store = new Vuex.Store({
    state: {
        profile: {},
        config: {},
        loggedIn: false,
        subscriptions: [],
        echo: null,
        showSidebar: false,
        showSidebarMD: false,
        showNavbar: false,
    },
    actions: {
        getProfile(context) {
            const profile = JSON.parse(window.userJSON);
            if (profile) {
                store.commit('setProfile', profile);
                store.commit('setLoggedIn');
            }
        },
        getConfig(context) {
            const config = JSON.parse(window.config);
            if (config) {
                store.commit('setConfig', config);
                store.dispatch('createEcho');
            }
        },
        getSubscriptions: (context) => {
            Vue.http.get('/api/subscriptions')
                .then(function(result) {
                    store.commit('setSubscriptions', result.body);
                }, function(response) {
                    // error
                });
        },
        createEcho: (context) => {
            const config = store.getters.getConfig;
            const echo = new Echo({
                broadcaster: config.broadcast.driver,
                key: config.broadcast.key,
                cluster: config.broadcast.cluster,
                encrypted: config.broadcast.encrypted
            });
            store.commit('setEcho', echo);
            store.dispatch('subscribeUpdates');
        },
        subscribeUpdates: () => {
            const echo = store.getters.getEcho;
            if (echo && store.getters.isLoggedIn) {
                const profile = store.getters.getProfile;
                echo.private('user.' + profile._id)
                    .listen('PostWasRead', function(data) {
                        const eventData = {
                            'post_id': data.postID,
                            'feed_id': data.feedID
                        };
                        store.commit('setRead', eventData);
                    })
                    .listen('SubscriptionCreated', function(data) {
                        store.commit('addSubscription', data.subscription);
                    }).listen('SubscriptionRemoved', function(data) {
                        store.commit('removeSubscription', data.subscriptionID);
                    });
            }
        },
        postRead: (context, eventData) => {
            if (store.getters.isLoggedIn) {
                const data = JSON.stringify({
                    post_id: eventData.post_id,
                    feed_id: eventData.feed_id
                });
                Vue.http.put('/api/subscriptions/read', data)
                    .then(function(result) {
                        store.commit('setRead', eventData);
                    }, function(response) {
                        // error
                    });
            }
        },
        toggleSidebar() {
            store.commit('toggleSidebar');
        },
        toggleNavbar() {
            store.commit('toggleNavbar');
        },
        hideSidebar() {
            store.commit('hideSidebar');
        },
        hideNavbar() {
            store.commit('hideNavbar');
        }
    },
    mutations: {
        updateProfile(state, profile) {
            for (const key in profile) {
                state.profile[key] = profile[key];
            }
        },
        setProfile(state, profile) {
            state.profile = profile;
        },
        setConfig(state, config) {
            state.config = config;
        },
        setLoggedIn(state) {
            state.loggedIn = true;
            store.dispatch('getSubscriptions');
            store.dispatch('subscribeUpdates');
        },
        setSubscriptions(state, subscriptions) {
            state.subscriptions.splice(0, state.subscriptions.length);
            for (const sub of subscriptions) {
                state.subscriptions.push(sub);
            }
        },
        addSubscription(state, subscription) {
            var alreadyExists = false;
            for (const key in state.subscriptions) {
                if (state.subscriptions[key]._id == subscription._id) {
                    alreadyExists = true;
                    break;
                }
            }
            if (!alreadyExists) {
                state.subscriptions.push(subscription);
            }
        },
        removeSubscription(state, subscriptionID) {
            var index = false;
            for (const key in state.subscriptions) {
                if (state.subscriptions[key]._id == subscriptionID) {
                    index = key;
                    break;
                }
            }
            if (index !== false) {
                state.subscriptions.splice(index, 1);
            }
        },
        setEcho(state, echo) {
            state.echo = echo;
        },
        setRead(state, eventData) {
            for (const sub of state.subscriptions) {
                if (sub.feed_id !== eventData.feed_id) {
                    continue;
                }
                const index = sub.unread_posts.indexOf(eventData.post_id);
                if (index !== -1) {
                    sub.unread_posts.splice(index, 1);
                }
            }

        },
        toggleSidebar(state) {
            state.showSidebar = !state.showSidebar;
            state.showSidebarMD = !state.showSidebarMD;
            state.showNavbar = false;
        },
        toggleNavbar(state) {
            state.showNavbar = !state.showNavbar;
            state.showSidebar = false;
        },
        hideSidebar(state) {
            state.showSidebar = false;
        },
        hideNavbar(state) {
            state.showNavbar = false;
        }
    },
    getters: {
        isLoggedIn: state => {
            return state.loggedIn;
        },
        getNicename: state => {
            return state.profile.nickname || state.profile.email || state.profile.name;
        },
        getProfile: state => {
            return JSON.parse(JSON.stringify(state.profile));
        },
        getSubscriptions: state => {
            return state.subscriptions;
        },
        getConfig: state => {
            return state.config;
        },
        getEcho: state => {
            return state.echo;
        },
        getShowSidebar: state => {
            return state.showSidebar;
        },
        getShowSidebarMD: state => {
            return state.showSidebarMD;
        },
        getShowNavbar: state => {
            return state.showNavbar;
        },
    }
});

module.exports = store;