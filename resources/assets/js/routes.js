const VueRouter = require('vue-router').default;

window.Vue.use(VueRouter);

const home = require('./components/Home.vue');
const search = require('./components/Search.vue');

const login = require('./components/user/Login.vue');
const profile = require('./components/user/Profile.vue');

const navigation = require('./components/subscription/Navigation.vue');
const subscription = require('./components/subscription/View.vue');

const feed = require('./components/feed/View.vue');
const feeds = require('./components/feed/List.vue');

const router = new VueRouter({
    mode: 'history',
    linkActiveClass: 'active',
    routes: [
        // dynamic segments start with a colon
        {
            path: '/',
            components: {
                default: home,
                sidebar: navigation
            },
            name: 'home'
        },
        {
            path: '/search',
            components: {
                default: search,
                sidebar: navigation
            },
            name: 'search'
        },
        {
            path: '/subscriptions/:id',
            components: {
                default: subscription,
                sidebar: navigation
            },
            name: 'subscription'
        },
        {
            path: '/feeds',
            components: {
                default: feeds,
                sidebar: navigation
            },
            name: 'feeds'
        },
        {
            path: '/feeds/:id',
            components: {
                default: feed,
                sidebar: navigation
            },
            name: 'feed'
        },
        {
            path: '/login',
            components: {
                default: login,
                sidebar: false
            },
            name: 'login'
        },
        {
            path: '/profile',
            components: {
                default: profile,
                sidebar: false
            },
            name: 'profile',
            // props: {default: {user}}
        }
    ]
});

module.exports = router;