const utils = require('./utils').default;

Vue.filter('highlight', function (value, highlight) {
    if (!value || typeof value !== 'string' || !highlight) {
        return value;
    }

    value = utils.escapeHtml(value);

    const regexText = highlight.split(' ').map(function(val) {
        return '(' + utils.escapeRegex(val) + ')';
    }).join('|');

    const regex = new RegExp(regexText, 'ig');

    return value.replace(regex, function (found) {
        return  '<mark>' + found + '</mark>';
    });
});